module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160251/learn_bootstrap/'
    : '/'
}
